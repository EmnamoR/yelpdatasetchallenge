### YelpChallenge Challenge


##### build docker 

docker build -t challenge:v0 .


##### run docker 
docker run -p 8890:8890 challenge:v0

##### open notebook

copy paste the token displayed on your terminal and access via browser


![docker run](docker_run.png)