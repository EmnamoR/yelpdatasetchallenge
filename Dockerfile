FROM jupyter/scipy-notebook
LABEL maintainer = "Amor Emna <emna.amor8@gmail.com>"

COPY notebooks work/notebooks
COPY models work/models
COPY tokenizers work/tokenizers
COPY requirements.txt work
RUN pip install  -r work/requirements.txt

WORKDIR work


EXPOSE 8890

CMD jupyter notebook --ip 0.0.0.0 --port 8890
